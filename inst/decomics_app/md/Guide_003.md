# Methods

 
- ICA : independent component analysis, a blind source separation algorithm: the gene expression matrix is separated into additive subcomponents. This is done by assuming that at most one subcomponent is Gaussian and that the subcomponents are statistically independent from each other. In DECOMICS ICA deconvolution is run using **fastICA** package (*R CRAN*) and the **Deconica** \[1\] R packages. By default, 30 significant gene markers are selected to get component scores, using the "weighted.mean" summary metric. 

 
- NMF : Non-negative matrix factorization, the gene expression matrix *X* is factorized into two matrices *A* and *T*, with the property that all three matrices have no negative elements. DECOMICS uses the *R CRAN* **NMF** package with method ="snmf/r". The estimated *A* matrix is constrained to sum the proportion to 1, and *T* is computed as *T= A^-1 . X* using the \textit{ginv} inverse function from **MASS** package. Finally, all negative values for *T* are set to *0*


 

- CDSeq \[2\] aims at simultaneously estimating *W* and *H* matrices using a probabilistic model based on latent Dirichlet allocation (LDA). DECOMICS uses the R implementation of the CDseq method CDSeqR with the following parameters : beta = 0.5, alpha = 5, mcmc\_iterations = 300. Reduction factor is computed to avoid expression values *> 10^5*, block numbers and gene block size are computed *s.t.*  a block do not exceed *10^3* genes 

 

- debCAM \[3\] stands for deconvolution by Convex Analysis of Mixtures. This approach use a geometric approach to identify a solution to the NMF problem in the simplex space. Thus, the proposed solution for *A* is always a proportion matrix. In DECOMICS, the function CAM is called from the debCAM R packages using the following empirical parameters : cluster.num is computed to be 5 times greater than the number of expected cell types, and  dim.rdc set to divide the number of input  genes by a tenth.    

 

- PREDE \[4\] is a recent method that offers the possibility to conduct partial reference-based deconvolution method solved via an iterative Quadratic Programming procedure. In DECOMICS, PREDE function in run from PREDE R package, with the following parameters : W1 = NULL (which corresponds to a complete deconvolution approach), type = "GE", iters = 100 and rssDiffStrop = 1e-5.

# Number of cell types

This module provides guidance in the determination of *K* , the number of
putative cell types from omics data (e.g. Gene expression matrix).

Cattell's rule is here suggested for choosing *K* : it states that
components corresponding to eigenvalues to the left of the straight line
should be retained. When the actual number of different cell types is
equal to *K*, we expect that there are (*K-1*) eigenvalues would correspond
to the mixture of cell types and that other eigenvalues would correspond
to noise (or other unaccounted for confounders). Indeed, one PCA axis is
needed to separate two types, two axes for three types, etc. However,
when not accounting for confounders, Cattell's rule overestimates the
number of PCs.

#### References {#references .western}

1. Urszula Czerwinska. UrszulaCzerwinska/DeconICA: DeconICA first release, May 2018. 

2. Kai Kang, Caizhi Huang, Yuanyuan Li, David M. Umbach,
and Leping Li. CDSeqR: fast complete deconvolution
for gene expression data from bulk tissues. BMC
Bioinformatics, 22(1):1–12, December 2021. Number: 1
Publisher: BioMed Central 

3. Lulu Chen, Chiung-Ting Wu, Niya Wang, David M.
Herrington, Robert Clarke, and Yue Wang. debCAM:
a bioconductor R package for fully unsupervised
deconvolution of complex tissues. Bioinformatics (Oxford,
England), 36(12):3927–3929, June 2020. 

4. Yufang Qin, Weiwei Zhang, Xiaoqiang Sun, Siwei Nan,
Nana Wei, Hua-Jun Wu, and Xiaoqi Zheng. Deconvolution
of heterogeneous tumor samples using partial reference
signals. PLoS computational biology, 16(11):e1008452,
November 2020.



