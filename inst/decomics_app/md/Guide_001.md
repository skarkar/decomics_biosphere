# **decomics - DEConvolution of OMICS data** 

#### *decomics* is a integrated tool dedicated to run *unsupervised* deconvolution of *bulk RNASeq*.
#### It provides several deconvolution algorithms of **Gene expression matrix**  as well as tools to analyse the results.
![]("www/deconvolution.jpg"){#Image1 align="bottom"
 height="252" border="1"}
 

# Terminology

1.  **Gene expression matrix** - it is a *k x m* matrix with *k* rows of
    genes and *m* columns of samples. Each data point in this matrix
    represents the expression of a given gene in a given sample

2.  **Gene signature matrix** - it is a *k x n* matrix with *n* rows of
    genes and *m* columns of cell fraction Each data point in this
    matrix represents the contribution of a gene towards a cell type

3.  **Cell proportion matrix** - it is a *n x m* matrix with *n* rows of
    cell types and *m* columns of samples. Each data point in this
    matrix represents the proportion of a given cell type in a given
    sample

# Usage

####  To run deconvolution on your **Gene expression matrix**, use the *"Deconvolution"*  tool.
Detailed help is located the [Input]() and [Methods]() guides 


####  To interpret the resulting  **Gene signature matrix**  and **Cell proportion matrix** , use the *"Analysis"*  tab.
Detailed help is located the [Enrichment Analysis]() and [Proportion Analysis]() guides 

### Aknowledgment 
[This is a contribution of the COMETH
program.](https://cancer-heterogeneity.github.io/cometh.html)
