#### NMF and ICA deconvolution methods (unsupervised methods) : {#nmf-and-ica-deconvolution-methods-unsupervised-methods .western}

**both inter- and intra- sample comparisons are possible** i.e.
comparing one cell-type within one sample and across samples is possible



#### References {#references .western}

Sturm et al (2019). \"Comprehensive evaluation of transcriptome-based
cell-type quantification methods for immuno-oncology.\" Bioinformatics.
https://doi.org/10.1093/bioinformatics/btz363

