# Input

-   Input your Gene expression matrix.

Upload gene expression matrix in the left panel using a specific csv
format. Use the CSV convertor module if necessary.

# Preprocessing

-   Normalization

RPM ; DESeq2, EdgeR

-   Transformation

log2 : y=log2(1+x)

-   Feature selection

Select variables (genes) according to their Coefficient of Variation
(CV)

cv1000 : select 1000 top genes

cv5000 : select 5000 top genes

# CSV convertor 

Need help with the format for your csv files ?

#

Try our CSV converter tool in the Convert/Import tab