This module helps in the biological interpretation of the components
identified by unsupervised approaches.

It proposes to perform Gene Set Enrichment analysis (fgsea R package
\[1\]). By defalut, the cell-type gene marker database is \'CellMatch\'
\[2\]\
To be noted: ICA-based methods are subjected to a reorientation of the
components, as proposed in the deconica R package \[3\].

We have included the following databases :
* GO : Gene Ontology database with biological and cellular processes
* GTEx :  a comprehensive Tissue Expression database
* KEGG : Regulation and Metablic Pathways  database
* Reactome : A comprehendive biological pathways database
* Tissue Cell Types : a curated database for identifyng common cell type 
* Cancer Cell Types : a curated database of cancer cell types
* Cancer Cell lines : a curated database of (cultivated) cancer cell lines 



#### References {#references .western}

\[1\] Korotkevich G, Sukhov V, Sergushichev A (2019). "Fast gene set
enrichment analysis." bioRxiv. doi: 10.1101/060012.\
\[2\] Shao et al., scCATCH:Automatic Annotation on Cell Types of
Clusters from Single-Cell RNA Sequencing Data, iScience, Volume 23,
Issue 3, 27 March 2020. doi: 10.1016/j.isci.2020.100882\
\[3\] https://urszulaczerwinska.github.io/DeconICA/

# Import Gene signature matrix  or previous results.
You can use your own gene signature matrix usingthe Convert/Import tab. 
Supported input files are: *.csv* files results *.rds* files

