Cattell's rule is here suggested for choosing K \[1\]: it states that
components corresponding to eigenvalues to the left of the straight line
should be retained. When the actual number of different cell types is
equal to K, we expect that there are (K-1) eigenvalues would correspond
to the mixture of cell types and that other eigenvalues would correspond
to noise (or other unaccounted for confounders). Indeed, one PCA axis is
needed to separate two types, two axes for three types, etc. However,
when not accounting for confounders, Cattell's rule overestimates the
number of PCs.

#### References {#references .western style="margin-top: 0.1in; margin-bottom: 0.1in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 110%; orphans: 2; widows: 2"}

\[1\] Cattell RB. The scree test for the number of factors. Multivariate
Behav Res. 2010;1:245--76.

##### Note : Only the 10,000 most variables features are kept for the analysis {#note-only-the-10000-most-variables-features-are-kept-for-the-analysis .western style="margin-top: 0.1in; margin-bottom: 0.1in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; line-height: 110%; orphans: 2; widows: 2"}
