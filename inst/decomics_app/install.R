BiocManager::install(c("markdown", "fgsea","NMF", "fastICA", "CDSeq", "debCAM"), update = FALSE)
remotes::install_github("Xiaoqizheng/PREDE",upgrade = FALSE)
remotes::install_github("bcm-uga/gedepir", upgrade = FALSE)
remotes::install_gitlab(repo = "Magali.Richard/decomics", host = "gitlab.in2p3.fr", upgrade=FALSE) 