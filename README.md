# Decomics : DEConvolution of OMICS data

An interactive Shiny app to run unsupervised deconvolution methods and analysis results using **gedepir** package : https://github.com/bcm-uga/gedepir

## Instalation


### Conda Environment

Build a R   conda environment  using Anaconda (https://www.anaconda.com/) or Miniconda (https://conda.io/miniconda.html) 

```
 conda create -n decomics -c bioconda -c r -c conda-forge  r-nmf r-fastica r-cdseq bioconductor-fgsea  bioconductor-debcam r-gtools r-matrixStats r-quadprog r-plotly r-ggplot2 r-cowplot  r-pheatmap r-markdown r-shinythemes r-remotes


conda activate decomics
```

### Installation of git R packages: 
Installation with  **remotes** is straightforward:
```
remotes::install_github("bcm-uga/gedepir")
remotes::install_github("Xiaoqizheng/PREDE")
remotes::install_gitlab(repo = "Magali.Richard/decomics", host = "gitlab.in2p3.fr") 

```

### Installation of R packages: 

Then, installation of packages in R is straightforward with  **remotes** and **BioConductor** :
```
BiocManager::install(c("fgsea","NMF","fastICA"),update = FALSE)
remotes::install_github("bcm-uga/gedepir", upgrade = TRUE)
remotes::install_gitlab(repo = "Magali.Richard/decomics", host = "gitlab.in2p3.fr") 
```
## Launch Shiny App in R : 
``` 
decomics::shiny_decomics()
```
or to get web link only : 
``` 
library(decomics) ; decomics::shiny_decomics(launch.browser=FALSE)
```

