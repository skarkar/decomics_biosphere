#' @export
shiny_decomics <-
  function(...) {
    appDir <-
      system.file("decomics_app", package = "decomics")
    shiny::runApp(appDir, display.mode = "normal",...)
  }

